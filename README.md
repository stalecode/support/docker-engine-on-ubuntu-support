# Docker Engine on Ubuntu Support

## Example

```text
$ make clean download
rm --force --recursive "download"
curl --create-dirs --fail --location --show-error --silent --output "download/containerd.io_1.6.32-1_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/containerd.io_1.6.32-1_amd64.deb"
curl --create-dirs --fail --location --show-error --silent --output "download/docker-buildx-plugin_0.14.0-1~ubuntu.24.04~noble_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/docker-buildx-plugin_0.14.0-1~ubuntu.24.04~noble_amd64.deb"
curl --create-dirs --fail --location --show-error --silent --output "download/docker-ce-cli_26.1.3-1~ubuntu.24.04~noble_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/docker-ce-cli_26.1.3-1~ubuntu.24.04~noble_amd64.deb"
curl --create-dirs --fail --location --show-error --silent --output "download/docker-ce-rootless-extras_26.1.3-1~ubuntu.24.04~noble_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/docker-ce-rootless-extras_26.1.3-1~ubuntu.24.04~noble_amd64.deb"
curl --create-dirs --fail --location --show-error --silent --output "download/docker-ce_26.1.3-1~ubuntu.24.04~noble_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/docker-ce_26.1.3-1~ubuntu.24.04~noble_amd64.deb"
curl --create-dirs --fail --location --show-error --silent --output "download/docker-compose-plugin_2.27.0-1~ubuntu.24.04~noble_amd64.deb" "https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/docker-compose-plugin_2.27.0-1~ubuntu.24.04~noble_amd64.deb"
```

```text
$ sudo make install
dpkg --install download/containerd.io_1.6.32-1_amd64.deb download/docker-buildx-plugin_0.14.0-1~ubuntu.24.04~noble_amd64.deb download/docker-ce-cli_26.1.3-1~ubuntu.24.04~noble_amd64.deb download/docker-ce-rootless-extras_26.1.3-1~ubuntu.24.04~noble_amd64.deb download/docker-ce_26.1.3-1~ubuntu.24.04~noble_amd64.deb download/docker-compose-plugin_2.27.0-1~ubuntu.24.04~noble_amd64.deb
[...]
Setting up containerd.io (1.6.32-1) ...
Setting up docker-buildx-plugin (0.14.0-1~ubuntu.24.04~noble) ...
Setting up docker-ce-cli (5:26.1.3-1~ubuntu.24.04~noble) ...
Setting up docker-ce-rootless-extras (5:26.1.3-1~ubuntu.24.04~noble) ...
Setting up docker-ce (5:26.1.3-1~ubuntu.24.04~noble) ...
Setting up docker-compose-plugin (2.27.0-1~ubuntu.24.04~noble) ...
Processing triggers for man-db (2.12.0-4build2) ...
$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
c1ec31eb5944: Pull complete 
Digest: sha256:266b191e926f65542fa8daaec01a192c4d292bff79426f47300a046e1bc576fd
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
[...]
```

```text
$ sudo make remove
dpkg --remove \
        containerd.io \
        docker-buildx-plugin \
        docker-ce \
        docker-ce-cli \
        docker-ce-rootless-extras \
        docker-compose-plugin
(Reading database ... 41222 files and directories currently installed.)
Removing docker-buildx-plugin (0.14.0-1~ubuntu.24.04~noble) ...
Removing docker-ce (5:26.1.3-1~ubuntu.24.04~noble) ...
Removing docker-ce-cli (5:26.1.3-1~ubuntu.24.04~noble) ...
Removing docker-ce-rootless-extras (5:26.1.3-1~ubuntu.24.04~noble) ...
Removing docker-compose-plugin (2.27.0-1~ubuntu.24.04~noble) ...
Removing containerd.io (1.6.32-1) ...
Processing triggers for man-db (2.12.0-4build2) ...
```

## Reference

[Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/#install-from-a-package)  
[Index of linux/ubuntu/dists/noble/pool/stable/amd64/](https://download.docker.com/linux/ubuntu/dists/noble/pool/stable/amd64/)  
