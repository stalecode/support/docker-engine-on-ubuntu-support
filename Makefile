CONTAINERD_VERSION ?= 1.7.19-1
BUILDX_VERSION ?= 0.16.1-1
DOCKER_VERSION ?= 27.1.1-1
COMPOSE_VERSION ?= 2.29.1-1

CODENAME ?= $(shell lsb_release -c --short)
HARDWARE ?= amd64

curl_command = curl --create-dirs --fail --location --show-error --silent --output "$(@)"

download: \
		download/containerd.io_$(CONTAINERD_VERSION)_$(HARDWARE).deb \
		download/docker-buildx-plugin_$(BUILDX_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce-cli_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce-rootless-extras_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-compose-plugin_$(COMPOSE_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb

.PHONY: clean
clean:
	rm --force --recursive "download"

download/containerd.io_%_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/containerd.io_$(*)_$(HARDWARE).deb"

download/docker-buildx-plugin_%~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/docker-buildx-plugin_$(*)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb"

download/docker-ce-cli_%~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/docker-ce-cli_$(*)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb"

download/docker-ce-rootless-extras_%~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/docker-ce-rootless-extras_$(*)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb"

download/docker-ce_%~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/docker-ce_$(*)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb"

download/docker-compose-plugin_%~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb:
	$(curl_command) "https://download.docker.com/linux/ubuntu/dists/$(CODENAME)/pool/stable/$(HARDWARE)/docker-compose-plugin_$(*)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb"

.PHONY: install
install: \
		download/containerd.io_$(CONTAINERD_VERSION)_$(HARDWARE).deb \
		download/docker-buildx-plugin_$(BUILDX_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce-cli_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce-rootless-extras_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-ce_$(DOCKER_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb \
		download/docker-compose-plugin_$(COMPOSE_VERSION)~ubuntu.24.04~$(CODENAME)_$(HARDWARE).deb
	dpkg --install $(^)

.PHONY: remove
remove:
	dpkg --remove \
		containerd.io \
		docker-buildx-plugin \
		docker-ce \
		docker-ce-cli \
		docker-ce-rootless-extras \
		docker-compose-plugin
